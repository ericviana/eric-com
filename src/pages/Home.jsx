import "../App.scss";

export function Home() {
  return (
    <div>
      <div className="hero">
        <h1>I Am Eric</h1>
        <h2>
          <del>Programmer</del>
          <br />
          <del>Engineer</del>
          <br />
          Maker.
        </h2>
      </div>
    </div>
  );
}

export default Home;
